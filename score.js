scoreController = function(){
    this.score = 0;
    this.snakeLength = 3;
    
    this.paint = function(_ctx){
        _ctx.font = "12px sans-serif";
        _ctx.fillText("SNAKE v0.1 | Score: " + this.score + "    Length: " + this.snakeLength, 4,13);
    
    }
    
    this.ping = function(){
        this.score+=15;
        this.snakeLength+=1;
    }


}