endgame = function(width, height, sc){
    
    var fps = 30;
    var introticker = null;
    var canvasE = null;
    var canvasC = null;
    var h = height;
    var w = width;
    var percent = 0;
    var sc_ = sc;
    
    this.paint = function(){
        canvasC.fillStyle = "#FFF";
        canvasC.font = "italic 70px sans-serif";
        canvasC.globalAlpha = 0.01;
        canvasC.fillText("YOU LOSE!", w/2-190,h/2+20);       
        canvasC.globalAlpha = 0.02;
        canvasC.fillText("YOU LOSE!", w/2-195,h/2+20);    
        
        canvasC.font = "italic 15px sans-serif";
        canvasC.fillText("Final Score: " + sc_.score, w/2-percent,h/2+50); 
        
        percent+=10;
        if(percent>100)percent=100;
    }
    
    this.create = function(){
        canvasE = document.getElementById("gamecanvas");
        canvasC = canvasE.getContext('2d');   
        introticker = setInterval(this.paint, 1000/fps);   
    }

}