fruitcontroller = function(){
    var fruitarray = [];                //existing fruits
    var fruiteffect = [];               //fading fruits
    this.sc = null;
    
    this.setScoreControl = function(_sc){
        this.sc = _sc;
    }
    
    this.deploynewfruit = function(_snake){
        var fx = Math.floor(Math.random()*25);
        var fy = Math.floor(Math.random()*25);
        
        var d = _snake.head;
        while(d.next!=null){
            if(fx==d.x && fy==d.y)return this.deploynewfruit(_snake);
            d = d.next;
        }
        fruitarray.push(new fruit(fx,fy,20));
    
    }
    
    this.fruitAt = function(_x,_y){
        for(var i = 0;i<fruitarray.length;i++){
            if((_x==fruitarray[i].x) && (_y==fruitarray[i].y)){
                return i;
            }
        }
        return -1;
    }
    
    this.eat = function(i){
        fruitarray[i].eat();
        fruiteffect.push(fruitarray[i]);
        fruitarray.splice(i,1);
        this.sc.ping();
    }

    this.paint = function(_ctx){
        for(var i = 0;i<fruitarray.length;i++)fruitarray[i].paint(_ctx);
        for(var i = 0;i<fruiteffect.length;i++){
            if(fruiteffect[i].paint){
                fruiteffect[i].paint(_ctx);
            }else{
                fruiteffect.splice(i,1);
                i--;
            }
        }
    }
    
    this.tick = function(){
        for(var i = 0;i<fruitarray.length;i++)fruitarray[i].tick();
        for(var i = 0;i<fruiteffect.length;i++){
            if(fruiteffect[i].tick){
                fruiteffect[i].tick();
            }        
        }
    }


}


fruit = function(x, y, ss){
    this.x = x;
    this.y = y;
    this.eaten = false;
    var squaresize = ss,
        radius = 6,
        desc = false,
        alpha = 1.0;
        
    this.paint = function(_ctx){        
        //faint background
        _ctx.fillStyle = "#FFF";
        _ctx.globalAlpha=(alpha>0.95)?alpha-0.95:0;
        _ctx.beginPath();
        _ctx.arc(x*ss+ss/2,y*ss+ss/2,radius*3,0,Math.PI*2,true);
        _ctx.closePath();
        _ctx.fill();
        
        //
        _ctx.globalAlpha=alpha;
        _ctx.strokeStyle = "#FFF";
        _ctx.beginPath();
        _ctx.arc(x*ss+ss/2,y*ss+ss/2,radius,0,Math.PI*2,true);
        _ctx.closePath();
        _ctx.stroke();
        
        _ctx.beginPath();
        _ctx.arc(x*ss+ss/2,y*ss+ss/2,radius-2,-Math.PI/2,Math.PI,true);
        _ctx.closePath();
        _ctx.stroke();
        
        _ctx.beginPath();
        _ctx.moveTo(x*ss+ss/2,y*ss+ss/2-radius);
        _ctx.lineTo(x*ss+ss/2+radius/2,y*ss+ss-radius*3);
        _ctx.stroke();
    }
    
    this.tick = function(){
        if(!desc || this.eaten){
            radius+=0.2;
        }else{
            radius-=0.2;
        }
        if(radius<7)desc = false;
        if(radius>9)desc = true;    
        
        if(this.eaten){
            alpha-=0.1;
            if(alpha<0){
                this.paint = null;
                this.tick = null;
            }
        }
    }

    this.eat = function(){        
        this.eaten = true;
        alpha = 1;
    }

}