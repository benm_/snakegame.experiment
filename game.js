var game = null;
var intro = null;
snakeGame = function(){
    var objects = new Array(3);
    var fps = 30;
    var gameticker = null;
    var canvasE = null;
    var canvasC = null;
    var isMoveTick = 1;
    
        
    this.tick = function(){
        for(var i=0;i<objects.length;i++){
            if(objects[i].tick!= null)objects[i].tick(isMoveTick);
        }   
        isMoveTick*=-1;
        for(var i=0;i<objects.length;i++){
            if(objects[i].paint!= null)objects[i].paint(canvasC);
        }  
    }
    
    this.create = function(){
        canvasE = document.getElementById("gamecanvas");
        canvasC = canvasE.getContext('2d');   
    
        objects[0] = (new gameBoard(25, 25, 20));    
        objects[1] = (new fruitcontroller());
        objects[2] = (new snake(4,4,20,-1,0,objects[1]));
        objects[3] = (new scoreController());
        
        objects[1].deploynewfruit(objects[2]);
        objects[1].deploynewfruit(objects[2]);
        objects[1].deploynewfruit(objects[2]);
        objects[1].setScoreControl(objects[3]);
        gameticker = setInterval(this.tick, 1000/fps);      
            
    }

    this.changeSnakeDirection = function(ndx, ndy){
        return objects[2].changeDirection(ndx,ndy);    
    }
    
    this.die = function(){
        clearInterval(gameticker);
        objects[0].paintdead(canvasC);
        objects[1].paint(canvasC);
        objects[2].paint(canvasC);
                
        var d = new endgame(500,500, objects[3]);
        d.create();
        
    }



}

//start here
function start(){
    intro = new introscreen(500,500);
    intro.create();
    document.onkeyup = watchForEnter;  
    
}

function createGame(){
    intro.destroy();
    
    document.onkeyup = keyPress;  
    game = new snakeGame();
    game.create();
    
}

function debug(log_txt) {
    if(window.console != undefined)console.log(log_txt);
}

function watchForEnter(e){
    var keyID = (window.event) ? event.keyCode : e.keyCode;
    if(keyID==13){
        createGame();
    }
    
}

function keyPress(e){
    var keyID = (window.event) ? event.keyCode : e.keyCode;
    switch(keyID){
        case 13:   //enter
            game.eatTHEFRUIT();
            if (e.preventDefault)
                    e.preventDefault();
            else e.returnValue = false;
            return false;      
        case 37:   //left
            game.changeSnakeDirection(-1,0);
            if (e.preventDefault)
                    e.preventDefault();
            else e.returnValue = false;
            return false;   
        case 38:   //up
            game.changeSnakeDirection(0,-1);
            if (e.preventDefault)
                    e.preventDefault();
            else e.returnValue = false;
            return false;   
        case 39:   //right
            game.changeSnakeDirection(1,0);
            if (e.preventDefault)
                    e.preventDefault();
            else e.returnValue = false;
            return false;   
        case 40:   //down
            game.changeSnakeDirection(0,1);
            if (e.preventDefault)
                    e.preventDefault();
            else e.returnValue = false;
            return false;   
    }    
}

function game_die(){
    game.die();

}
