snake = function(headx, heady, ss, dx, dy, fc){
    this.head = null;
    this.grow = false;
    var squaresize = ss,
        directionx = dx*-1,
        directiony = dy*-1,
        fruitcon = fc;
    
     
    this.snakesegment = function(_x,_y){
        this.x = _x;
        this.y = _y;
        this.next = null;
        
        this.paint = function(_ctx, isHead){
            _ctx.strokeStyle = "#FFF";
            _ctx.beginPath();
            if(isHead){                
                
                _ctx.arc(this.x*squaresize+squaresize/2,this.y*ss+squaresize/2,squaresize/2+1,0,Math.PI*2,true);
                _ctx.closePath();
                _ctx.stroke();
                _ctx.globalAlpha=0.5;
                _ctx.fill();
                
            }else{
                _ctx.arc(this.x*squaresize+squaresize/2,this.y*ss+squaresize/2,squaresize/2,0,Math.PI*2,true);
                _ctx.closePath();
                _ctx.stroke();
            }
            
        }
    }
    
    this.collidesWith = function(x,y){
        
    }
    
    this.paint = function(_ctx){
        this.head.paint(_ctx, true);
        var h = this.head.next;
        h.paint(_ctx);
        while(h.next!=null){
            h = h.next;
            h.paint(_ctx);
        }
    }
    
    this.tick = function(isMove){
        if(isMove==1)this.advance();
        
    }
    
    this.destroyLast = function(){
        var n = this.head;
        while(n.next!=null){
            if(n.next.next == null){
                n.next = null;
            }else{
                n = n.next;
            }            
        }
    }
    
    this.advance = function(){
        var newx = this.head.x + directionx,
            newy = this.head.y + directiony;
            
        if(newx>=25)newx=0;
        if(newx<0)newx=24;
        if(newy>=25)newy=0;
        if(newy<0)newy=24;
        
        //check collision
        var hh = this.head;
        while(hh.next!=null){            
            hh = hh.next;
            if((newx==hh.x) && (newy ==hh.y)){
                game_die();
                return;
            }
        }        
        //
        
        var newhead = new this.snakesegment(newx,newy);
        newhead.next = this.head;
        this.head = newhead;
        if(!this.grow)this.destroyLast();    
        this.grow = false;
        var ii = fruitcon.fruitAt(newx, newy);
        
        if(ii>=0){
            fruitcon.eat(ii);
            fruitcon.deploynewfruit(this);
            this.grow = true;
        }   
        
        return 1;
    }
    
    this.changeDirection = function(nx, ny){
        var newx = this.head.x + nx,
            newy = this.head.y + ny;
           
        if(newx>=25)newx=0;
        if(newx<0)newx=24;
        if(newy>=25)newy=0;
        if(newy<0)newy=24;
            
        if((newx == this.head.next.x)&&(newy == this.head.next.y))return false;    
        directionx = nx;
        directiony = ny;
        return true;        
    }
    
    this.head = new this.snakesegment(headx,heady);
    this.head.next = new this.snakesegment(this.head.x+dx, this.head.y+dy);
    this.head.next.next = new this.snakesegment(this.head.next.x+dx, this.head.next.y+dy);
    


}