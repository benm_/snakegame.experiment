introscreen = function(width, height){

    var fps = 30;
    var introticker = null;
    var canvasE = null;
    var canvasC = null;
    var h = height;
    var w = width;
    var percent = 0;

    
    
    this.paint = function(){
        if(percent==0){
            canvasC.globalAlpha = 1;
            canvasC.fillStyle = "#003153";
            canvasC.fillRect(0,0,w, h);        
            
            
        
            percent++;
        }else{
        
            canvasC.globalAlpha = 0.1;
            canvasC.fillStyle = "#003153";
            canvasC.fillRect(0,0,w, h);  
        
        
            canvasC.fillStyle = "#FFF";
            canvasC.font = "italic 120px sans-serif";
            canvasC.globalAlpha = Math.abs(Math.sin(percent/5)*0.5)*(percent/100);
            canvasC.fillText("SNAKE", w/2-200,h/2+40);       
            canvasC.globalAlpha = Math.abs(Math.sin(percent/5+Math.PI/2)*0.5)*(percent/100);
            canvasC.fillText("SNAKE", w/2-205,h/2+40); 
            
            canvasC.font = "italic 15px sans-serif";
            canvasC.fillText("Press Enter to Start!", w/2-percent,h/2+70); 

            var rx = Math.floor(Math.random()*25);
            var ry = Math.floor(Math.random()*25);
            
            canvasC.strokeStyle = "#FFF";
            canvasC.beginPath();
            canvasC.arc(rx*20+10,ry*20+10,10,0,Math.PI*2,true);
            canvasC.closePath();
            canvasC.stroke();
            
            percent+=2;
            if(percent>100)percent=100;
            }
    }
   
    this.destroy = function(){
        clearInterval(introticker);
    }
   

    this.create = function(){
        canvasE = document.getElementById("gamecanvas");
        canvasC = canvasE.getContext('2d');   
        introticker = setInterval(this.paint, 1000/fps);   
    }





}